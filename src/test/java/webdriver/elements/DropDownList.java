package webdriver.elements;

import org.openqa.selenium.By;

public class DropDownList extends BaseElement {

	public DropDownList(final By locator, final String name) {
		super(locator, name);
	}

	public DropDownList(String string, String name) {
		super(string, name);
	}



	public DropDownList(By locator) {
		super(locator);
	}

	protected String getElementType() {
		return getLoc("loc.label");
	}

}
