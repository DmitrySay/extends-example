package webdriver;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * An abstract class that describes the basic test application contains
 * methods for logging and field test settings (options)
 */
public abstract class BaseTest extends BaseEntity {


    /**
     * To override.
     */
    public abstract void runTest();

    /**
     * Test
     *
     * @throws Throwable Throwable
     */
    @Test
    @Parameters({"appUrl"})
    public void xTest(String appUrl) throws Throwable {
        Class<? extends BaseTest> currentClass = this.getClass();

        try {
            logger.logTestName(currentClass.getName());
            browser.navigate(appUrl);
            browser.windowMaximise();
            //browser.navigate(Browser.getBaseUrl()); //urlLoginPage in selenium.properties
            runTest();
            logger.logTestEnd(currentClass.getName());
        } catch (Throwable e) {

            logger.warn("");
            logger.warnRed(getLoc("loc.test.failed"));
            logger.warn("");
            logger.fatal(e.getMessage());
        }

    }

    /**
     * Format logging
     *
     * @param message Message
     * @return Message
     */
    protected String formatLogMsg(final String message) {
        return message;
    }

}
