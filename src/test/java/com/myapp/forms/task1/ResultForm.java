package com.myapp.forms.task1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import webdriver.BaseForm;
import webdriver.elements.Label;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class ResultForm extends BaseForm {

    private By searchResultsLocator = By.xpath("//div[@class='search-i']/ol/descendant::li//h3/a[2]");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public ResultForm() {
        super(By.xpath("//a[contains(@href,'http://search.tut.by/')]"), "Search page ");
    }


    /**
     * Method gets  list of headers' text
     * @return  headers
     */
    private List<String> getResultHeaders() {
        List<WebElement> searchResults = new Label(searchResultsLocator).getElements();
        List<String> headers = new ArrayList<>();
        for (WebElement element : searchResults) {
            headers.add(element.getText());
        }
        return headers;
    }

    /**
     * В методе осуществляется проверка результатов поиска на соответствие
     * заданному регулярному выражению regexp. Метод возвращает список строк,
     * соответствующих регулярному выражению. Проверка осуществляется только для
     * первой страницы результатов поиска.
     *
     * @param regexp -regular expression
     *
     */
    public void verifySearchResults(String regexp) {

        List<String> searchResults = getResultHeaders();
        List<String> compilantResults = new ArrayList<>();

        Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        //Pattern.CASE_INSENSITIVE. CASE_INSENSITIVE означает, что при поиске выражения, регистр символов не будет учитываться.

        for (String header : searchResults) {
            if (pattern.matcher(header).matches()) {
                compilantResults.add(header);
            }
        }

        logger.info(String.format("%d results is found", compilantResults.size()));
        if (!compilantResults.isEmpty()) {
            for (String result : compilantResults) {
                logger.info(String.format("%s", result));
            }
        }

    }

    /**
     * В методе происходит открытие новой страницы, переход на нее
     * и закрытие страницы при существовании ссылки с абсолютным
     * соответствием заданному выражению searchexp.Иначе, бросается исключение
     *
     * @param searchexp
     */
    public void findAndGoToSearchExp(String searchexp) {

        try {
            String originalWindow = browser.getDriver().getWindowHandle();
            final Set<String> oldWindowsSet = browser.getDriver().getWindowHandles();

            //переход по ссылке, если есть заданное выражение
            browser.getDriver().findElement(By.linkText(searchexp)).click();

            String newWindow = (new WebDriverWait(browser.getDriver(), 10))
                    .until(new ExpectedCondition<String>() {
                               public String apply(WebDriver driver) {
                                   Set<String> newWindowsSet = driver.getWindowHandles();
                                   newWindowsSet.removeAll(oldWindowsSet);
                                   return newWindowsSet.size() > 0 ?
                                           newWindowsSet.iterator().next() : null;
                               }
                           }
                    );

            browser.getDriver().switchTo().window(newWindow);
            logger.info("New window title: " + browser.getDriver().getTitle());
            browser.getDriver().close();

            browser.getDriver().switchTo().window(originalWindow);
            logger.info("Old window title: " + browser.getDriver().getTitle());


        } catch (WebDriverException e) {
            System.out.println(e);
            logger.info("Result is not found...");

        }
    }

}

