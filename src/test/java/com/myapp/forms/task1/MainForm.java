package com.myapp.forms.task1;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.TextBox;

public class MainForm extends BaseForm {

    private TextBox txbSearchBar = new TextBox(By.id("search_from_str"), "Search bar");
    private Button btnSubmitSearch = new Button(By.xpath("//input[@name='search']"), "Search");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public MainForm() {
        super(By.id("search_from_str"), "Tut.by");
    }

    /**
     * Method gets query text, type text in search field
     * And after click on button- move to the next page
     * @param text
     */
    public void searchFor(String text) {
        txbSearchBar.type(text);
        btnSubmitSearch.click();
        browser.waitForPageToLoad();

    }

}
