package com.myapp.forms.task2;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.Link;
import webdriver.elements.TextBox;

public class LoginForm extends BaseForm {

    private TextBox txbLoginInput = new TextBox(By.id("Email"), "login input");
    private Button btnSubmit = new Button(By.id("next"), "Next");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public LoginForm() {
        super(By.id("Email"), "Login page Google");
    }

    /**
     * Fill in login
     * @param login
     */
    public void setLogin(String login) {
        txbLoginInput.type(login);
        btnSubmit.click();
    }


}


