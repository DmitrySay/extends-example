package com.myapp.forms.task2;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Link;

public class MainForm extends BaseForm {

    private Link mailLink = new Link(By.linkText("Почта"), "Mail");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public MainForm() {
        super(By.id("hplogo"), "Google.by");
    }

    /**
     * Search and go to page by link email
     */
    public void navigateToMail() {
         mailLink.click();
    }

}
