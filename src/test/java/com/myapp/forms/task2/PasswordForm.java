package com.myapp.forms.task2;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.TextBox;

public class PasswordForm extends BaseForm {

    private TextBox txbLoginInput = new TextBox(By.id("Passwd"), "password input");
    private Button btnSubmit = new Button(By.id("signIn"), "SIGN IN");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public PasswordForm() {
        super(By.id("Passwd"), "Password page Google");
    }

    /**
     * Fill in password
     * @param password
     */
    public void setPassword(String password) {
        txbLoginInput.type(password);
        btnSubmit.click();
    }


}


