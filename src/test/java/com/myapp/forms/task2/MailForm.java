package com.myapp.forms.task2;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.Link;
import webdriver.elements.TextBox;

public class MailForm extends BaseForm {

    private TextBox txbTo = new TextBox(By.xpath("//textarea[contains(@aria-label, 'Кому')]"), "To");
    private TextBox subjectbox = new TextBox(By.name("subjectbox"), "subject");
    private TextBox txbMessage = new TextBox(By.xpath("(//*[@aria-label='Тело письма'])[2]"), "message");
    private Button btnWrite = new Button(By.xpath("//div[text()= 'НАПИСАТЬ']"), "to write");
    private Button btnSend = new Button(By.xpath("//div[text()='Отправить']"), "SEND");


    /**
     * Default Constructor
     * Check is if page correct
     */
    public MailForm() {
        super(By.xpath("//div[text()= 'НАПИСАТЬ']"), "Mail page Google");
    }

    /**
     * Create and send email
     *
     * @param email   -кому отправляем
     * @param topic   -тема письма
     * @param message -текст письма
     */
    public void setEmailTopicMessageAndSend(String email, String topic, String message) {
        btnWrite.click();
        browser.waitForPageToLoad();
        txbTo.type(email);
        subjectbox.type(topic);
        txbMessage.type(message);
        btnSend.click();
        browser.waitForPageToLoad();
    }

    /**
     * Logout from email
     */
    public void logoutFromMail() {
        WebElement element = browser.getDriver().findElement(By.id("gb_71"));
        JavascriptExecutor executor = (JavascriptExecutor) browser.getDriver();
        executor.executeScript("arguments[0].click();", element);
        logger.info(String.format("Logout from email Google"));

    }

    /**
     * В методе осуществляется переход между страницами Входящие/Отправленные
     * и проверка на наличие название в title при открытии страницы.
     * Результат выводиться в консоль
     * @param text- название Входящие/Отправленные
     */
    public void navigateBoxesMail(String text) {

        Link link = new Link(By.partialLinkText(text), text);
        link.isPresent(60);
        link.click();
        link.isPresent(60);
        logger.info("Set text is =" + text);
        title = "";

        title = browser.getTitle();

        if (title.contains(text)) {
            logger.info(String.format("Title is: %s", browser.getTitle()));
            logger.info(String.format("The title contains %s", text));

        } else {
            logger.info(String.format("Title is: %s", browser.getTitle()));
            logger.info(String.format("The title does not contain %s", text));

        }

    }


}


