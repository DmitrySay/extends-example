package com.myapp.forms.task3;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;

public class CreditCardForm extends BaseForm {


    /**
     * Default Constructor
     * Check is if page correct
     */
    public CreditCardForm() {
        super(By.id("continue_button"), "CREDIT CARD INFO PAGE");
    }

    /**
     * Method check if button
     * Complete Purchase is active
     */
    public void checkIfBtnActive() {

        Button btnPurchase = new Button(By.id("continue_button"), "Complete Purchase");
        assert (btnPurchase.isEnabled());
        assert (btnPurchase.isPresent());
        logger.info("Button 'Complete Purchase' is active ");
    }

}

