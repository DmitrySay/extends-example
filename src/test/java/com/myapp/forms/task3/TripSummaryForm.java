package com.myapp.forms.task3;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;

public class TripSummaryForm extends BaseForm {


    private Button btn = new Button(By.xpath("//button[@id='tripSummarySubmitBtn']"), "CONTINUE");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public TripSummaryForm() {
        super(By.xpath("//button[@id='tripSummarySubmitBtn']"), "TRIP SUMMARY PAGE");
    }

    /**
     * Click on button to continue next page
     */
    public void clickOnBtnContinue() {
        btn.isPresent(30);
        btn.click();
        logger.info(String.format("Click on 'Continue' button"));

    }


}
