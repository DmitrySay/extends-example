package com.myapp.forms.task3;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.Link;
import webdriver.elements.TextBox;

public class MainForm extends BaseForm {

    private Link lnkFlightTab = new Link(By.xpath("//a[@id='book-air-content-trigger']"), "Flight tab");
    private Link lnkROUNDTRIP = new Link(By.xpath("//label[@id='roundTripBtn']/span"), "ROUND TRIP");
    private TextBox txbFrom = new TextBox(By.xpath("//input[@id='originCity']"), "FROM");
    private TextBox txbTo = new TextBox(By.xpath("//input[@id='destinationCity']"), "TO");
    private TextBox txbDepartDate = new TextBox(By.xpath("//input[@id='departureDate']"), "DEPART DATE");
    private TextBox txbReturnDate = new TextBox(By.xpath("//input[@id='returnDate']"), "RETURN DATE");
    private Link lnkED = new Link(By.xpath("//label[@id='exactDaysBtn']/span"), "EXACT DATES");
    private Link lnkCash = new Link(By.xpath("//label[@id='cashBtn']/span"), "SHOW PRICE IN MONEY");
    private Button btnFind = new Button(By.name("findFlightsSubmit"), "FIND FLIGHTS");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public MainForm() {
        super(By.xpath("//a[@id='book-air-content-trigger']"), "delta.com");
    }

    /**
     * Select Flight tab
     */
    public void clickOnFlightTab() {
        lnkFlightTab.click();
        logger.info("Click on 'FLIGHT' tab under 'Book a trip' booking widget");
    }

    /**
     * Select Flight type =RoundTrip
     */
    public void clickOnRoundTrip() {
        lnkROUNDTRIP.click();
        logger.info("Click on 'ROUND TRIP' tab under 'Book a trip' booking widget");

    }

    /**
     * Fill in FROM/TO fly destination
     *
     * @param from
     * @param to
     */
    public void setFromAndToFields(String from, String to) {
        txbFrom.type(from);
        logger.info(String.format("SET FROM: %s", from));
        txbTo.type(to);
        logger.info(String.format("SET TO: %s", to));

    }

    /**
     * Fill in DepartDate/ReturnDate
     *
     * @param departDate
     * @param returnDate
     */
    public void setDepartDateAndReturnDate(String departDate, String returnDate) {
        departDate.replaceAll("[\\D]", "");
        returnDate.replaceAll("[\\D]", "");
        txbDepartDate.type(departDate);
        logger.info(String.format("SET DEPART DATE: %s", departDate));
        txbReturnDate.type(returnDate);
        logger.info(String.format("SET RETURN DATE: %s", returnDate));
    }

    /**
     * Select Date type =ExactDates
     */
    public void clickOnExactDates() {
        lnkED.click();
        logger.info(String.format("Click on 'ROUND TRIP' tab under 'Book a trip' booking widget"));
    }

    /**
     * Select Price type =SHOW PRICE IN:Money
     */
    public void clickOnShowPriceInMoney() {
        lnkCash.click();
        logger.info(String.format("Click on 'SHOW PRICE IN Money' tab under 'Book a trip' booking widget"));
    }

    /**
     * Click on submit button
     */
    public void clickOnFindFlightsSubmit() {
        btnFind.click();
        logger.info(String.format("Click on 'FindFlightsSubmit' in booking widget"));
    }
}
