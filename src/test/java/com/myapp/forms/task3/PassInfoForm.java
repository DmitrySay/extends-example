package com.myapp.forms.task3;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import webdriver.BaseForm;
import webdriver.elements.Button;
import webdriver.elements.DropDownList;
import webdriver.elements.TextBox;

import java.util.List;

public class PassInfoForm extends BaseForm {

    private Button btnPrefix = new Button((By.xpath("//span[@id='prefix0-button']/span")), "prefix button");
    private Button btnContinue = new Button((By.id("paxReviewPurchaseBtn")), "continue button");
    private DropDownList prefixList = new DropDownList(By.xpath("//ul[@id='prefix0-menu']/descendant::li"));
    private Button btnGender = new Button((By.xpath("//span[@id='gender0-button']/span")), "gender button");
    private DropDownList genderList = new DropDownList(By.xpath("//ul[@id='gender0-menu']/descendant::li"));
    private Button btnMonth = new Button((By.xpath("//span[@id='month0-button']/span")), "month button");
    private DropDownList monthList = new DropDownList(By.xpath("//ul[@id='month0-menu']/descendant::li"));
    private Button btnDay = new Button((By.xpath("//span[@id='day0-button']/span")), "day button");
    private DropDownList dayList = new DropDownList(By.xpath("//ul[@id='day0-menu']/descendant::li"));
    private Button btnYear = new Button((By.xpath("//span[@id='year0-button']/span")), "yaer button");
    private DropDownList yearList = new DropDownList(By.xpath("//ul[@id='year0-menu']/descendant::li"));
    private TextBox txbFirstName = new TextBox(By.name("firstName0"), "FirstName");
    private TextBox txbLastName = new TextBox(By.name("lastName0"), "LastName");
    private TextBox txbEmail = new TextBox(By.id("email"), "EMAIL");
    private TextBox txbREEmail = new TextBox(By.id("reEmail"), "CONFIRM EMAIL");
    private TextBox txbPhone = new TextBox(By.id("telephoneNumber0"), "telephone number");

    //=======DEFAUL PARAMETERS======//
    private String prefix = "Miss";
    private String firstname = "Vasil";
    private String lastname = "Pupkin";
    private String gender = "Male";
    private String month = "December";
    private String phonenumber = "123456789";
    private String email = "12345@mail.by";
    private String day = "25";
    private String year = "1980";
    //===============================//


    /**
     * Default Constructor
     * Check is if page correct
     */
    public PassInfoForm() {
        super(By.id("paxReviewPurchaseBtn"), "PASSANGER INFO PAGE");
    }

    /**
     * In this method fill in field prefix.
     * Prefix can be change on any from list
     *
     * @param prefix
     */
    public void fillPrefixBox(String prefix) {
        btnPrefix.click();
        String flag = "false";
        List<WebElement> searchResults = prefixList.getElements();
        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(prefix)) {
                searchResults.get(i).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            logger.info(String.format("Please check if the %s prefix exist or correct", prefix));
        } else {
            logger.info(String.format("SET PREFIX: %s", prefix));
        }
    }


    /**
     * In this method fill in Gender
     * Parameter can be change on any from list
     *
     * @param gender
     */
    public void fillGenderBox(String gender) {

        btnGender.click();
        String flag = "false";
        List<WebElement> searchResults = genderList.getElements();

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(gender)) {
                searchResults.get(i).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            logger.info(String.format("Please check if the %s Gender exist or correct", gender));
        } else {
            logger.info(String.format("SET Gender: %s", gender));
        }

    }


    /**
     * In this method fill in Month
     * Parameter can be change on any from list
     *
     * @param month
     */
    public void fillMonthBox(String month) {
        btnMonth.click();
        String flag = "false";
        List<WebElement> searchResults = monthList.getElements();

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(month)) {
                searchResults.get(i).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            logger.info(String.format("Please check if the %s month exist or correct", month));
        } else {
            logger.info(String.format("SET Month: %s", month));
        }

    }

    /**
     * In this method fill in Day
     * Parameter can be change on any from list
     *
     * @param day
     */
    public void fillDayBox(String day) {

        btnDay.click();
        String flag = "false";
        List<WebElement> searchResults = dayList.getElements();

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(day)) {
                searchResults.get(i).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            logger.info(String.format("Please check if the %s Day exist or correct", day));
        } else {
            logger.info(String.format("SET Day: %s", day));
        }

    }

    /**
     * In this method fill in YEAR
     * Parameter can be change on any from list
     *
     * @param year
     */

    public void fillYearBox(String year) {

        btnYear.click();
        String flag = "false";
        List<WebElement> searchResults = yearList.getElements();

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(year)) {
                searchResults.get(i).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            logger.info(String.format("Please check if the %s year exist or correct ", year));
        } else {
            logger.info(String.format("SET Year: %s", year));
        }

    }

    /**
     * In this method fill in fields FIRST NAME/LAST NAME.
     * Parameters can be change on any from list
     *
     * @param firstname
     * @param lastname
     */
    public void fillFirstAndLastNameBox(String firstname, String lastname) {
        txbFirstName.type(firstname);
        txbLastName.type(lastname);
    }

    /**
     * Fill in Telephone Number
     *
     * @param phonenumber
     */
    public void fillPhNumberBox(String phonenumber) {
        txbPhone.type(phonenumber);

    }

    /**
     * Fill in email
     *
     * @param email
     */
    public void setAndConfirmEmailBox(String email) {
        txbEmail.type(email);
        txbREEmail.type(email);
    }

    /**
     * Turn off emergancy fields
     */
    public void turnOFFEmergencyContact() {
        WebElement element = browser.getDriver().findElement(By.id("declineContactN_0"));
        JavascriptExecutor executor = (JavascriptExecutor) browser.getDriver();
        executor.executeScript("arguments[0].click();", element);
        logger.info(String.format("Select NO in emergency contact info"));
    }

    /**
     * Click to continue
     */
    public void clickONContinueBtn() {
        btnContinue.click();
    }

    /**
     * At this method Fill in all required
     * Passenger Information fields
     */
    public void fillDefaultFieldsInPassInfoPage() {
        fillPrefixBox(prefix);
        fillFirstAndLastNameBox(firstname, lastname);
        fillGenderBox(gender);
        fillMonthBox(month);
        fillDayBox(day);
        fillYearBox(year);
        fillPhNumberBox(phonenumber);
        setAndConfirmEmailBox(email);
        turnOFFEmergencyContact();
        clickONContinueBtn();

    }

}
