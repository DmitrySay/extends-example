package com.myapp.forms.task3;

import org.openqa.selenium.By;
import webdriver.BaseForm;
import webdriver.elements.Button;

public class TicketsForm extends BaseForm {


    private Button btnSelect = new Button(By.xpath("//button[@id='0_0_0']"), "SELECT");

    /**
     * Default Constructor
     * Check is if page correct
     */
    public TicketsForm() {
        super(By.xpath("//button[@id='0_0_0']"), "Tickets selection page");
    }

    /**
     * Select the 1st ticket
     */
    public void selectTicket() {
        btnSelect.isPresent(60);
        btnSelect.click();
        logger.info(String.format("Click on select button with ticket"));
    }
}
