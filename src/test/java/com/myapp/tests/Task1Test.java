package com.myapp.tests;

import com.myapp.forms.task1.MainForm;
import com.myapp.forms.task1.ResultForm;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import webdriver.BaseTest;

public class Task1Test extends BaseTest {

    protected String request;
    protected String regexp;
    protected String searchexp;


    @BeforeMethod
    @Parameters({"request", "regexp", "searchexp"})
    public void setParams(String request, String regexp, String searchexp) {
        this.request = request;
        this.regexp = regexp;
        this.searchexp = searchexp;

    }


    public void runTest() {

        logger.step(1);
        MainForm mainForm = new MainForm();

        logger.step(2);
        mainForm.searchFor(request);

        logger.step(3);
        ResultForm resultForm = new ResultForm();

        logger.step(4);
        resultForm.verifySearchResults(regexp);

        logger.step(5);
        resultForm.findAndGoToSearchExp(searchexp);


    }
}
