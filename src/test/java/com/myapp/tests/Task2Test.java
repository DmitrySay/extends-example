package com.myapp.tests;

import com.myapp.forms.task2.LoginForm;
import com.myapp.forms.task2.MailForm;
import com.myapp.forms.task2.MainForm;
import com.myapp.forms.task2.PasswordForm;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import webdriver.BaseTest;

public class Task2Test extends BaseTest {

    protected String login;
    protected String password;
    protected String email;
    protected String topic;
    protected String message;

    @BeforeMethod
    @Parameters({"login", "password", "email", "topic", "message"})
    public void setParams(String login, String password, String email, String topic, String message) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.topic = topic;
        this.message = message;

    }

    public void runTest() {

        logger.step(1);
        MainForm mainForm = new MainForm();
        logger.step(2);
        mainForm.navigateToMail();

        logger.step(3);
        LoginForm loginForm = new LoginForm();
        logger.step(4);
        loginForm.setLogin(login);

        logger.step(5);
        PasswordForm passwordFom = new PasswordForm();
        logger.step(6);
        passwordFom.setPassword(password);

        logger.step(6);
        MailForm mailForm = new MailForm();
        logger.step(7);
        mailForm.setEmailTopicMessageAndSend(email,topic, message);
        logger.step(8);
        mailForm.navigateBoxesMail("Отправленные");
        logger.step(9);
        mailForm.navigateBoxesMail("Входящие");
        logger.step(10);
        mailForm.logoutFromMail();
    }
}
