package com.myapp.tests;


import com.myapp.forms.task3.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import webdriver.BaseTest;

public class Task3Test extends BaseTest {

    protected String flightType;
    protected String from;
    protected String to;
    protected String departDate;
    protected String returnDate;
    protected String travelDate;
    protected String showPriceIn;

    @BeforeMethod
    @Parameters({"flightType", "from", "to", "departDate", "returnDate", "travelDate", "showPriceIn"})
    public void setParams(String flightType,
                          String from,
                          String to,
                          String departDate,
                          String returnDate,
                          String travelDate,
                          String showPriceIn) {
        this.flightType = flightType;
        this.from = from;
        this.to = to;
        this.departDate = departDate;
        this.returnDate = returnDate;
        this.travelDate = travelDate;
        this.showPriceIn = showPriceIn;

    }

    public void runTest() {

        logger.step(1);
        MainForm mainForm = new MainForm();
        logger.step(2);
        mainForm.clickOnFlightTab();

        logger.step(3);
        if (flightType.equals("ROUND TRIP")) {
            mainForm.clickOnRoundTrip();
        }
        logger.step(4);
        mainForm.setFromAndToFields(from, to);
        logger.step(5);
        mainForm.setDepartDateAndReturnDate(departDate, returnDate);

        logger.step(6);
        if (travelDate.equals("EXACT DATES")) {
            mainForm.clickOnExactDates();
        }
        logger.step(7);
        if (showPriceIn.equals("Money")) {
            mainForm.clickOnShowPriceInMoney();
        }
        logger.step(8);
        mainForm.clickOnFindFlightsSubmit();

        logger.step(9);
        TicketsForm ticketsForm = new TicketsForm();
        logger.step(10);
        ticketsForm.selectTicket();
        logger.step(11);
        ticketsForm.selectTicket();

        logger.step(12);
        TripSummaryForm tripSummaryForm = new TripSummaryForm();
        logger.step(13);
        tripSummaryForm.clickOnBtnContinue();

        logger.step(14);
        PassInfoForm passwordForm = new PassInfoForm();
        logger.step(15);
        passwordForm.fillDefaultFieldsInPassInfoPage();

        logger.step(16);
        CreditCardForm creditCardForm = new CreditCardForm();
        logger.step(17);
        creditCardForm.checkIfBtnActive();

    }
}
